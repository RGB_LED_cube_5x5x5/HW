# ==<| RGB LED cube 5x5x5 |>==
## About
 * Schematic and PCB were designed at Eagle 5, but PDF versions are attached
   as well
 * MCU: ATmega64
 * To reduce power consumption, DC/DC changer is used -> wide range of external
   power supply voltage.
 * On board FTDI chip is for optional communication with PC
 * Keyboard is optional as well. In default cube will play animations in
   infinite loop.

## Download
 * For downloading, please use GIT command with "--recursive" parameter

   $ git clone --recursive https://gitlab.com/RGB_LED_cube_5x5x5/RGB_LED_cube_5x5x5.git

## Help and support
 * If you have questions, or if you want to contribute on this project, just
   let me know [martin.stej at gmail dot com]